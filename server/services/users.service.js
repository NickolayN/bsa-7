const usersRepository = require('../repositories/users.repository');

function getUsers() {
  return usersRepository.getUsers();
}

function  getUser(login) {
  if (typeof parseInt(login) === 'number') {
    return usersRepository.getUserById(login);
  }
  return usersRepository.getUser(login);
}

const addUser = (message) => {
  return usersRepository.addUser({...message});
};

const deleteUser = (id) => {
  return usersRepository.deleteUser(id);
};

const updateUser = (id, data) => {
  return usersRepository.updateUser(id, data);
};

module.exports = {
  getUser,
  getUsers,
  addUser,
  updateUser,
  deleteUser
};