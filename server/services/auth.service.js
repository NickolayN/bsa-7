const jwt = require('jsonwebtoken');
const {verifyUser, getUserByLogin} = require('../helpers/socket.helper');

function login(user) {
  const userFromDB = verifyUser(user);
  if (userFromDB) {
    const token = jwt.sign(user, 'secret');
    return {
      token,
      ...userFromDB
    };
  }
  
  return null;
}

module.exports = {
  login
}