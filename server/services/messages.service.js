const messagesRepository = require('../repositories/messages.repository');

function getId() {
  return String(Date.now());
}

function getMessages() {
  return messagesRepository.getMessages();
}

function  getMessage(id) {
  return messagesRepository.getMessage(id);
}


const addMessage = (message) => {
  return messagesRepository.addMessage({...message, id: getId()});
};

const deleteMessage = (id) => {
  return messagesRepository.deleteMessage(id);
};

const updateMessage = (id, data) => {
  return messagesRepository.updateMessage(id, data);
};

module.exports = {
  getMessage,
  getMessages,
  addMessage,
  updateMessage,
  deleteMessage
};