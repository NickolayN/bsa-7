const {getUserFromSocket} = require('../helpers/socket.helper');

function socketMiddleware(socket, next) {
  try {
    getUserFromSocket(socket);
    return next();
  } catch (error) {
    socket.disconnect();
  } 
}

module.exports = socketMiddleware;