const fs = require('fs');

const writeData = (data) => {
  const json = JSON.stringify(data);
  fs.writeFile('./data/users.json',json, (err) => {
    if (err) {
      console.log(err);
      return null;
    } else {
      return data;
    }
  });
};

function getUsers() {
  const rawData = fs.readFileSync('./data/users.json');
  const usersList = JSON.parse(rawData);

  return usersList;
}

function getUserById(id) {
  const usersList = getUsers();

  return usersList.find(user => user.id === id);
}

function getUser(login) {
  const usersList = getUsers();

  return usersList.find(user => user.user === login);
}

const addUser = (user) => {
  const users = getUsers();

  users.push(user);
  writeData(users);

  return user;
};

const deleteUser = (id) => {
  const users = getUsers();
  const index = users.findIndex(user => user.id == id);
  if (!index) {
    return false;
  } else {
    users.splice(index, 1);
    writeData(users);
    return true;
  }
  
}

const updateUser = (id, data) => {
  const users = getUsers();

  const updatedUsers = users.map(item => {
    if (item.user === id) {
      return {
        ...item,
        ...data
      };
    }
    return item;
  });

  writeData(updatedUsers);
  
  return true;
}

module.exports = {
  getUser,
  getUsers,
  addUser,
  updateUser,
  deleteUser,
  getUserById
};