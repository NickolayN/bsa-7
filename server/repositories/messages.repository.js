const fs = require('fs');

const writeData = (data) => {
  const json = JSON.stringify(data);
  fs.writeFile('./data/messages.json',json, (err) => {
    if (err) {
      console.log(err);
      return null;
    } else {
      return data;
    }
  });
};

function getMessages() {
  const rawData = fs.readFileSync('./data/messages.json');
  const messagesList = JSON.parse(rawData);

  return messagesList;
}

function getMessage(id) {
  const messagesList = getMessages();

  return messagesList.find(message => message.id === id);
}

const addMessage = (message) => {
  const messages = getMessages();

  messages.push(message);
  writeData(messages);

  return message;
};

const deleteMessage = (id) => {
  const messages = getMessages();
  const index = messages.findIndex(message => messages.id == id);
  if (!index) {
    return false;
  } else {
    messages.splice(index, 1);
    writeData(messages);
    return true;
  }
  
}

const updateMessage = (id, data) => {
  const messages = getMessages();

  const updatedMessages = messages.map(item => {
    if (item.id === id) {
      return {
        ...item,
        ...data
      };
    }
    return item;
  });

  writeData(updatedMessages);
  
  return true;
}

module.exports = {
  getMessages,
  getMessage,
  addMessage,
  updateMessage,
  deleteMessage
}