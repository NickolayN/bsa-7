const userRepository = require('../repositories/users.repository');
const jwt = require('jsonwebtoken');


function verifyUser(user) {
  const userInDB = userRepository.getUser(user.login);
  return (userInDB && userInDB.password === user.password) ? userInDB : null;
}

function getUserFromSocket(socket) {
  const token = socket.handshake.query.token;
  return user = jwt.verify(token, 'secret');
}

module.exports = {verifyUser, getUserFromSocket};