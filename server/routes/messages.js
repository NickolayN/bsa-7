const express = require('express');
const router = express.Router();
// const authMiddleware = require('../middlewares/auth.middleware');

const {
  getMessages,
  getMessage,
  addMessage,
  updateMessage,
  deleteMessage
} = require('../services/messages.service');

router.get('/', (req, res, next) => {
  const users = getMessages();
  res.json(users)
});

router.get('/:id',  (req, res, next) => {
  const id = req.params.id;
  const user = getMessage(id);
  res.json(user);
});

router.post('/', (req, res, next) => {
  const result = addMessage(req.body);
  if (!result) {
    res.status(400).send('Error. Failed to add message');
  } else {
    res.send(result);
  } 
});

router.put('/:id', (req, res, next) => {
  const id = req.params.id;
  const data = req.body;

  const result = updateMessage(id, data);
  if (!result) {
    res.status(400).send('Error. Failed to update message');
  } else {
    res.send(result);
  }  
});

router.delete('/:id', (req, res, next) => {
  const id = req.params.id;
  const result = deleteMessage(id);
  if (!result) {
    res.status(400).send('Failed to delete message');
  } else {
    res.send(`Deleted Message N${id}`);
  }  
});

module.exports = router;