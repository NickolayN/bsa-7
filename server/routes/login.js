const express = require('express');
const router = express.Router();
const authService = require('../services/auth.service');

router.post('/', (req, res, next) => {
  const userFromReq = req.body;
  const data = authService.login(userFromReq);
  

  if(data) {
    res.status(200).json({auth: true, ...data});
  } else {
    res.status(401).json({auth: false});
  }
});

module.exports = router;