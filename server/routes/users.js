const express = require('express');
const router = express.Router();
// const authMiddleware = require('../middlewares/auth.middleware');

const usersService = require('../services/users.service');

router.get('/', (req, res, next) => {
  const users = usersService.getUsers();
  res.json(users)
});

router.get('/:id',  (req, res, next) => {
  const id = req.params.id;
  const user = usersService.getUser(id);
  res.json(user);
});

router.post('/', (req, res, next) => {
  const result = usersService.addUser(req.body);
  if (!result) {
    res.status(400).send('Error. Failed to add user');
  } else {
    res.send(result);
  } 
});

router.put('/:id', (req, res, next) => {
  const id = req.params.id;
  const data = req.body;

  const result = usersService.updateUser(id, data);
  if (!result) {
    res.status(400).send('Error. Failed to update user');
  } else {
    res.send(result);
  }  
});

router.delete('/:id', (req, res, next) => {
  const id = req.params.id;
  const result = usersService.deleteUser(id);
  if (!result) {
    res.status(400).send('Failed to delete user');
  } else {
    res.send(`Deleted User N${id}`);
  }  
});

module.exports = router;