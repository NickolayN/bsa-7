import React from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Chat from './containers/chat/index';
import loginPage from './containers/loginPage/index';
import EditMessagePage from './containers/editMessagePage';
import UserList from './containers/userList/index';
import PrivateRoute from './components/privateRoute/privateRoute';
import UserPage from './containers/userPage/index';

function App() {
    return (
      <div className="App">
        <Switch>
          <Route exact path='/login' component={loginPage} />
          <PrivateRoute exact path='/' component={Chat} /> 
          <PrivateRoute path='/message/:id' component={EditMessagePage} /> 
          <PrivateRoute exact path='/users' component={UserList} />
          <PrivateRoute exact path='/user' component={UserPage} /> 
          <PrivateRoute path='/user/:id' component={UserPage} />          
        </Switch>
      </div>
    );
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.loginPage
  };
}

export default connect(mapStateToProps)(App);
