import { combineReducers } from 'redux';
import chat from '../containers/chat/reducer';
import editMessagePage from '../containers/editMessagePage/reducer';
import loginPage from '../containers/loginPage/reducer';
import users from '../containers/userList/reducer';
import userPage from '../containers/userPage/reducer';

const rootReducer = combineReducers({
  loginPage,
  chat,
  editMessagePage,
  users,
  userPage
});

export default rootReducer;