import { all } from 'redux-saga/effects';
import loginPageSaga from '../containers/loginPage/sagas';
import chatSaga from '../containers/chat/sagas';
import usersSagas from '../containers/userList/sagas';
import userPageSagas from '../containers/userPage/sagas';
import editMessageSagas from '../containers/editMessagePage/sagas';

export default function* rootSaga() {
    yield all([
        loginPageSaga(),
        chatSaga(),
        usersSagas(),
        userPageSagas(),
        editMessageSagas()
    ])
};