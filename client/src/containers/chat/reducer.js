import {
  LIKE_MESSAGE,
  FETCH_MESSAGES_SUCCESS,
  SET_LOADING
} from "./actionTypes";

const initialState = {
  messages: [],
  isLoading: true
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LOADING: {
      const {isLoading} = action.payload;
      return {
        ...state,
        isLoading
      };
    }

    case LIKE_MESSAGE: {
      const { id } = action.payload;
      const updatedMessages = state.messages.map(message => {
        if (message.id === id) {
          const isLiked = !message.isLiked;
          return {
            ...message,
            isLiked
          };
        }
        return message;
      });
      return {
        ...state,
        messages: updatedMessages
      };
    }

    case FETCH_MESSAGES_SUCCESS: {
      const messages = action.payload;
      return {
        ...state,
        messages,
        isLoading: false
      }
    }

    default:
      return state;
  }
}