import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  FETCH_MESSAGES,
  ADD_MESSAGE,
  FETCH_MESSAGES_SUCCESS,
  SET_LOADING,
  DELETE_MESSAGE,
  UPDATE_MESSAGE,
  LIKE_MESSAGE
} from "./actionTypes";

const apiUrl = 'http://localhost:5000/api/';

export function* fetchMessages(action) {
  try {
    yield put({ type: SET_LOADING, payload: {isLoading: true} });
    const response = yield call(axios.get, `${apiUrl}messages`);
    yield put({ type: FETCH_MESSAGES_SUCCESS, payload: response.data });
  } catch (error) {
    console.log(error);
  }
}

function* watchFetchMessages(action) {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

function* addMessage(action) {
  const newMessage = action.payload;

  try {
    yield put({ type: SET_LOADING, payload: {isLoading: true} });
    yield call(axios.post, `${apiUrl}messages`, newMessage);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.log(error)
  }
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE, addMessage);
}

function* deleteMessage(action) {
  const id = action.payload.id;
  try {
    yield put({ type: SET_LOADING, payload: {isLoading: true} });
    yield call(axios.delete, `${apiUrl}messages/${id}`);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.log(error)
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

function* updateMessage(action) {
  const { id, data } = action.payload;

  try {
    yield put({ type: SET_LOADING, payload: {isLoading: true} });
    yield call(axios.put, `${apiUrl}messages/${id}`, data);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.log(error)
  }
}

function* watchUpdatingMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

function* likeMessage(action) {
  const { id } = action.payload;

  try {
    yield put({ type: SET_LOADING, payload: {isLoading: true} });
    const response = yield call(axios.get, `${apiUrl}messages/${id}`);
    const message = response.data;
    const isLiked = !message.isLiked;
    const updatedMessage = {...message, isLiked};
    yield put({type: UPDATE_MESSAGE, payload: {id, data: updatedMessage}});
  } catch (error) {
    console.log(error);
  }
}

function* watchLikeMessage() {
  yield takeEvery(LIKE_MESSAGE, likeMessage);
}

export default function* chatSaga() {
  yield all([
    watchFetchMessages(),
    watchAddMessage(),
    watchDeleteMessage(),
    watchUpdatingMessage(),
    watchLikeMessage()
  ]);
};