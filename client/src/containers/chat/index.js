import React from "react";
import { connect } from 'react-redux';
import * as actions from './actions';
import Header from '../../components/header/header';
import MessagesList from '../../components/messagesList/messagesList';
import MessageForm from '../../components/messageForm/messageForm';
import Loader from '../../components/loader/loader';

import './index.css';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.onDelete = this.onDelete.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onLike = this.onLike.bind(this);
  }

  onAdd(message) {
    const user = this.props.currentUser;
    this.props.addMessage({...user, ...message});
  }

  onDelete(id) {
    this.props.deleteMessage(id);
  }

  onEdit(id) {
    this.props.history.push(`/message/${id}`);
  }

  onLike(id) {
    this.props.likeMessage(id);
  }

  countParticipants(messages) {
    const participants = new Set();
    messages.forEach(message => {
      if (!participants.has(message.user)) {
        participants.add(message.user);
      }
    });

    return participants.size;
  }

  componentDidMount() {
    this.props.fetchMessages();
  }

  renderLoading() {
    return <Loader />;
  }

  renderChat() {
    const chat = this.props.chat;
    return (
      <div className="Chat">
        <header>
          <Header
            participants={this.countParticipants(chat.messages)}
            messages={chat.messages.length}
            lastMessageDate={chat.messages[chat.messages.length - 1]['created_at']}
          />
        </header>
        <MessagesList
          messages={chat.messages}
          currentUser={this.props.currentUser}
          onDelete={this.onDelete}
          onEdit={this.onEdit}
          onLike={this.onLike}
        />
        <MessageForm
          onAdd={this.onAdd}
        />
      </div>
    )
  }

  render() {
    const chat = this.props.chat;

    if(this.props.isLoading) {
      return this.renderLoading();
    }

    return this.renderChat(chat);
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
    currentUser: state.loginPage.currentUser,
    isLoading: state.chat.isLoading
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);