import {
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  FETCH_MESSAGES,
  SET_LOADING
} from "./actionTypes";
import service from './service';

export const setLoading = () => ({
  type: SET_LOADING,
  payload: {
    isLoading: true
  }
});

export const addMessage = data => ({
  type: ADD_MESSAGE,
  payload: {
    id: service.getMessageId(),
    'created_at': service.getDate(),
    ...data
  }
});

export const updateMessage = (id, data) => ({
  type: UPDATE_MESSAGE,
  payload: {
      id,
      data
  }
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: {
      id
  }
});

export const likeMessage = id => ({
  type: LIKE_MESSAGE,
  payload: {
    id
  }
});

export const fetchMessages = () => ({
  type: FETCH_MESSAGES
});