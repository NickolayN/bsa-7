import React, { Component } from "react";
import { connect } from 'react-redux';
import UserItem from '../../components/userItem/userItem';
import * as actions from './actions';
import Loader from '../../components/loader/loader';

class UserList extends Component {
  constructor(props) {
    super(props);
    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onAdd = this.onAdd.bind(this);
  }

  componentDidMount() {
    this.props.fetchUsers();
  }

  onEdit(id) {
    this.props.history.push(`/user/${id}`);
  }

  onDelete(id) {
    this.props.deleteUser(id);
  }

  onAdd() {
    this.props.history.push('/user');
  }

  renderLoader() {
    return <Loader />
  }

  renderUsersList() {
    return (
      <div className="user-list">
        <div className="user-list-header">
          <button onClick={() => this.props.history.push('/')}>Chat</button>
        </div>
        <div className="list-group col-10">
          {
            this.props.users.map(user => {
              return (
                <UserItem
                  key={user.id}
                  id={user.id}
                  name={user.user}
                  surname={user.surname}
                  email={user.email}
                  onEdit={this.onEdit}
                  onDelete={this.onDelete}
                />
              );
            })
          }
        </div>
        <div className="col-2">
          <button
            className="btn btn-success"
            onClick={this.onAdd}
            style={{ margin: "5px" }}
          >
            Add user
					</button>
        </div>
      </div>
    );
  }

  render() {
    if (this.props.isLoading) {
      return this.renderLoader();
    }

    return this.renderUsersList();
  }
}


const mapStateToProps = (state) => {
  return {
    users: state.users.usersList,
    isLoading: state.users.isLoading
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);