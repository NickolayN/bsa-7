import { FETCH_USERS_SUCCESS, SET_LOADING } from "./actionTypes";

export default function (state = {isLoading: true, usersList: []}, action) {
  switch (action.type) {
    case FETCH_USERS_SUCCESS: {
      return {
        isLoading: false,
        usersList: [...action.payload.users]
      };
    }

    case SET_LOADING: {
      const {isLoading} = action.payload;
      return {
        ...state,
        isLoading
      };
    }

    default:
      return state;
  }
}
