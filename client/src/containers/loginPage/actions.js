import { AUTH_REQUEST } from './actionTypes';

export const authorize = (payload) => ({
  type: AUTH_REQUEST,
  payload
});
