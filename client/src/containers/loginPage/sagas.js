import { call, put, takeEvery, all } from 'redux-saga/effects';
import axios from 'axios';
import { AUTH_REQUEST, AUTH_SUCCESS, AUTH_FAILURE } from './actionTypes';

export function* authorize(action) {
  try {
    const response = yield call(axios.post, 'http://localhost:5000/login', action.payload);
    yield put({type: AUTH_SUCCESS, payload: response.data});
    // localStorage.setItem('jwt', response.token);
  } catch (error) {
    console.log(error);
  }
}

function* watchAuthorize() {
  yield takeEvery(AUTH_REQUEST, authorize);
}

export default function* loginPageSagas() {
  yield all([
    watchAuthorize()
  ]);
};