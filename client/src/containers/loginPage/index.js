import React from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';
import Login from '../../components/login/login';

import './index.css';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
    this.onSubmit = this.onSubmit.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.currentUser.user && props.currentUser.isAdmin) {
      props.history.push('/users');
    } else if (props.currentUser.user) {
      props.history.push('/');
    }
    return null;
  }


  onSubmit(payload) {
    this.props.authorize(payload);
  }

  render() {
    return (
      <div>
        <Login onSubmit={this.onSubmit} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.loginPage.currentUser,
    isAdmin: state.loginPage.isAdmin
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);