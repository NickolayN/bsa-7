import { AUTH_SUCCESS } from './actionTypes';

const initialState = {
  currentUser: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case AUTH_SUCCESS: {
      const userData = action.payload;
      return {
        ...state,
        currentUser: userData
      }
    }

    default:
      return state;
  }
}