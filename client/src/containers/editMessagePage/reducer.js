import { FETCH_MESSAGE_SUCCESS } from "./actionTypes";

const initialState = {
  id: ''
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_MESSAGE_SUCCESS:
      const { message } = action.payload;
      return {
        ...state,
        ...message
      };
  
    default:
      return state;
  }
}