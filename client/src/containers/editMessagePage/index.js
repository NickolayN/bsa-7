import React from "react";
import { connect } from 'react-redux';
import * as actions from './actions';
import Textarea from '../../components/textarea/textarea';
import { updateMessage } from "../chat/actions.js";

import './index.css';

class EditMessagePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.fetchMessage(this.props.match.params.id)
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.id !== state.id) {
      const message = props.messages.find(message => props.id === message.id);
      if (message) {
        return {
          message: message.message,
          id: props.id
        };
      }
    }
    return null;
  }

  onChange(e) {
    const message = e.target.value;
    this.setState(
      {
        ...this.state,
        message
      }
    );
  }

  onCancel() {
    this.setState({ message: '', id: '' });
    this.props.history.push('/');
  }

  onSave() {
    this.props.updateMessage(this.props.id, this.state);
    this.props.history.push('/');
    this.setState({ message: '', id: '' });
  }

  getEditMessagePageContent() {
    return (
      <div className="modal">
        <div className="modal-content">
          <h2>Edit message</h2>
          <Textarea
            value={this.state.message}
            onChange={(e) => this.onChange(e)}
          />
          <div className="modal-footer">
            <button onClick={this.onCancel}>Cancel</button>
            <button onClick={this.onSave}>Save</button>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return this.getEditMessagePageContent();
  }
}

const mapStateToProps = (state) => {
  return {
    message: state.editMessagePage.message,
    id: state.editMessagePage.id,
    messages: state.chat.messages
  }
};

const mapDispatchToProps = {
  ...actions,
  updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessagePage);