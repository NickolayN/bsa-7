import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_MESSAGE, FETCH_MESSAGE_SUCCESS } from "./actionTypes";

const apiUrl = 'http://localhost:5000/api';

export function* fetchMessage(action) {
  try {
    const messageData = yield call(axios.get, `${apiUrl}/messages/${action.payload.id}`);
    yield put({ type: FETCH_MESSAGE_SUCCESS, payload: { message: messageData.data } })
  } catch (error) {
    console.log('fetchUsers error:', error.message)
  }
}

function* watchFetchMessage() {
  yield takeEvery(FETCH_MESSAGE, fetchMessage)
}

export default function* editMessageSagas() {
  yield all([
    watchFetchMessage()
  ])
};