import React from 'react';
import Textarea from '../textarea/textarea';

import './messageForm.css';

class MessageForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const message = e.target.value;
    this.setState({
      ...this.state,
      message
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onAdd(this.state);
    this.setState({
      ...this.state,
      message: ''
    });
  }

  render() {
    return (
      <form className="messageForm" onSubmit={this.onSubmit}>
        <Textarea
          value={this.state.message}
          onChange={ this.onChange }
        />
        <button type="submit">Send</button>        
      </form>
    )
  }
}

export default MessageForm;