import React from 'react';
import Message from '../message/message';
import './messageList.css';

class MessagesList extends React.Component {
  componentDidUpdate = () => { 
    this.node.scrollTop = this.node.scrollHeight;
  }
  
  componentDidMount = () => {
    this.node.scrollTop = this.node.scrollHeight;
  }

  createSeparator(date) {
    return <li
      className="separator"
      key={date.getTime()}
    >
      {date.toDateString()}
    </li>
  }

  createMessage(message) {
    if (message.user === this.props.currentUser.user) {
      return (
        <Message
          key={message.id}
          isOwn={true}
          {...message}
          onDelete={this.props.onDelete}
          onEdit={this.props.onEdit}
        />
      )
    } else {
      return (
        <Message
          key={message.id}
          {...message}
          onEdit={this.props.onEdit}
          onLike={this.props.onLike}
        />
      )
    }
  }

  createList(messages) {
    return messages.reduce((prev, next) => {
      if (prev.length === 0) {
        
        const nextDate = new Date(next['created_at']);
        prev.push(this.createSeparator(nextDate));
      }

      if (prev.length >= 1) {
        
        const prevDateStr = prev[prev.length - 1].props['created_at'];
        const nextDateStr = next['created_at'];
        const prevDate = new Date(prevDateStr);
        const nextDate = new Date(nextDateStr);
        if (nextDate.getDate() > prevDate.getDate()) {
          prev.push(this.createSeparator(nextDate));
        }
      }
      
      prev.push(this.createMessage(next));
      return prev;
    }, []);
  }

  render() {
    const messages = this.props.messages;
    return (
      <ul className="Messages-list" ref={node => this.node = node}>
        {
          this.createList(messages)
        }
      </ul>
    );
  }
}

export default MessagesList;