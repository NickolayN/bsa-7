import React from 'react';
import './header.css';

function Header(props) {
  const formatDate = (date) => {
    return new Date(date).toLocaleTimeString();
  }
  const { participants, messages, lastMessageDate } = props;

  return (
    <div className="Header">
      <div>participants: {participants}</div>
      <div>messages: {messages}</div>
      <div>lastMessage: {formatDate(lastMessageDate)}</div>
    </div>
  )
}

export default Header;