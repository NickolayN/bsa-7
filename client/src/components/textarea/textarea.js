import React from 'react';
import './textarea.css';

class Textarea extends React.Component {
  autosize = () => {
    this.node.height = 'auto';
    this.node.style.height = this.node.scrollHeight + 'px';
  }

  componentDidMount() {
    this.autosize();
    this.node.focus();
  }

  handleChange(e) {
    e.preventDefault();
    this.autosize(e);
    this.props.onChange(e);
  }
  render() {
    return (
      <textarea
        value={this.props.value}
        ref={node => this.node = node}
        className="MessageInput"
        rows="1"
        onChange={(e) => this.handleChange(e)}
      />
    )
  }
}

export default Textarea;