import React from 'react';
import './loader.css';

function Loader() {
  return (
    <div className="Loader"></div>
  )
}

export default Loader;