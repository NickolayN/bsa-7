import React from 'react';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const target = e.target;
    const name = target.name;
    const value = target.value;

    this.setState({
      [name]: value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(this.state);
  }

  render() {
    return (
      <div className="login-page">
        <form className="login-form">
          <input
            name="login"
            value={this.state.login}
            type="text"
            onChange={this.onChange}
          />
          <input
            name="password"
            value={this.state.password}
            type="text"
            onChange={this.onChange}
          />
          <button
            type="submit"
            onClick={(e) => this.onSubmit(e)}
          >
            Submit
          </button>
        </form>
      </div>
    )
  }
}

export default Login;