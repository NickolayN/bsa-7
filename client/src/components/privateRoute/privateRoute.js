import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ isAuthorized, location, ...props }) => (
  isAuthorized
      ? <Route {...props} render={({ Component, ...rest }) => <Component {...rest} />} />
      : <Redirect to={{ pathname: '/login', state: { from: location } }} />
);

PrivateRoute.defaultProps = {
  isAuthorized: false,
  location: undefined,
};

const mapStateToProps = state => ({
  isAuthorized: state.loginPage.currentUser.auth
});

export default connect(mapStateToProps)(PrivateRoute);