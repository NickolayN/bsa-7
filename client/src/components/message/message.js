import React, {useState} from 'react';
import Avatar from '../avatar/avatar';
import MessageContent from '../messageContent/messageContent';
import EditButton from '../editButton/editButton';

import './message.css';

function Message(props) {
  const [isEditButtonShown, toggle] = useState(false);

  const getEditButton = (id) => {
    return isEditButtonShown ?
      <EditButton
        onClick={(e) => props.onEdit(id)}
      /> :
      null;
  }

    let className = props.isOwn ? "message current-member" : "message";
    className = props.isLiked ? className + " Liked" : className;
    const id = props.id;

  if (props.isOwn) {
    return (
      <li
        className={className}
        onMouseEnter={() => toggle(true)}
        onMouseLeave={() => toggle(false)}
      >
        <MessageContent {...props} />
        <button
        className="message-action"
          onClick={(e) => props.onDelete(id)}
        >
          Delete
        </button>
        {getEditButton(id)}
      </li>
    )
  }

  return (
    <li
      className={className}
      onMouseEnter={() => toggle(true)}
      onMouseLeave={() => toggle(false)}
    >
      <Avatar
        avatar={props.avatar}
        user={props.user}
      />
      <MessageContent {...props} />
      {getEditButton(id)}
      <button className="Like-button" onClick={(e) => props.onLike(id)}>Like</button>
    </li>
  )
}

export default Message;