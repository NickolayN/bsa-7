import React from "react";
import './userItem.css';

export default function UserItem(props) {
  const { id, name, surname, email } = props;
  return (
    <div className="user-item">
      <div className="user-item__info">
        <span className="user-item__name">{name} {surname}</span>
        <span className="user-item__email">{email}</span>
      </div>
      <div className="user-item__actions">
        <button className="btn btn-outline-primary" onClick={(e) => props.onEdit(id)}> Edit </button>
        <button className="btn btn-outline-dark" onClick={(e) => props.onDelete(id)}> Delete </button>
      </div>
    </div>
  );
};