import React from 'react';

function MessageContent(props) {
  const formatDate = (date) => {
    const dateObj = new Date(date);
    return dateObj.toLocaleTimeString();
  }

  return (
    <div className="Message-content">
      <span className="Author">{props.user}</span>
      <div className="Message-text">
        {props.message}
      </div>
      <time dateTime={props['created_at']}>
        {formatDate(props['created_at'])}
      </time>
    </div>
  );
}

export default MessageContent;